# Short documentation for AML-Extended improvements

## Source and target vocabularies
The original version of AML gave symmetrical roles to the 2 vocabularies.
In this extended version i) the already matched terms ('skos:exactMatch' triples) are loaded from the source file
and ii) the definitions exported after alignment come from the target vocabulary.

## Saving alignment results
After the alignments have been generated and some of them 'Set Correct', you can save the alignement (menu: file/save alignment).


![figure 1: std_align_result](doc_extended_images/std_align_result.png "figure 1")


In the screenshot given (see figure 1),  3 alignments are marked as valid.

If you save this alignment in alignment.tsv (be carefull when selecting the format/extension), you will obtain 2 more files as shown in the table below: 

| File name | Description |
|-----------|-------------|
|alignment.tsv|this file stores the score for each proposed alignment, and the list of validated ones ('set correct' button)|
|alignment.tsv.DEFINITIONS| this file keeps track of the requested definitions |
|alignment.tsv.DEFINITIONS.rdf |the RDF triples with the definitions inherited from the target thesaurus. Triples can be directly merged with the source thesaurus. |
|alignment.tsv.EXACTMATCH.rdf| the RDF triples with the exactMatch relation for all the matching concepts. Can be directly merged with the source thesaurus.|

## Loading alignements already present in the source vocabulary

Result from a previous alignment can be uploaded in AML.
This operation restores matching results and validated match. 

Moreover, the alignements between the source vocabulary and the target vocabulary and present in the source vocabulary as 'skos:exactMatch' triples can be identified through the menu Match/ExistingMatch. These matches can be masked using the  "mask/display" button.

## Diplaying definitions
Definition of the concepts,  when provided by one or both vocabularies, are displayed by a mouse-over on elements of the left column of the Alignment Panel


## Updating alignment (adding definition)
After selecting the check box for new definition, you have to click the "set correct" button. You can then save the alignment.
