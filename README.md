# AgreementMakerLight Extented (AML-E)

## What is AgreementMarkerLight Extented
AgreementMarkerLight is defined as "AgreementMaker is one of the leading ontology matching systems, thanks to its combination of a flexible and extensible framework with a comprehensive user interface."

The extended version of AML is a fork from https://github.com/AgreementMakerLight/AML-Project on github. We have made  some improvements decribed in the [documentation extended version](doc_extended.md) file available at the project's root. As a reminder, at the end of this readme file, you'll find the licence for the original version.

The main improvements are:

* added capability to load already integrated matchings (exacMatch that were inserted in the source ontology after a previous alignment) and to display or masks them in the Alignment Panel
* export definitions as RDF triples if available in the target ontology (TSV format) . Therefore, you can add these new definitions in the source ontology.
* export 'skos:exactMatch' triples (TSV format). Therefore, you can add these triples in the source ontology.


## Installing AML-E

Use these commands to download AML-E sources and generate jar file.

 ```bash
 git clone https://forgemia.inra.fr/christian.pichot/AML-Project.git
 cd AML-Project/AgreemenMarkerLight
 mvn package
 ```
This will generate a file: target/aml-2.1-Extended.jar

## Running AML-E in user interface mode

To run AML's user interface, you just have to execute the application with no command line arguments.
This can be done by double-clicking the file (if it has execution permissions) or by typing:

    java -jar target/aml-2.1-Extended.jar


## Running AML-E in command line mode

To run AML-E in command line mode, you have to execute the application with command line arguments:
    
    java -jar target/aml-2.1-Extended.jar OPTIONS

Where the OPTIONS are:

    -s (--source) 'path_to_source_ontology'     Specifies the source ontology file (mandatory)
    -t (--target) 'path_to_target_ontology'     Specifies the target ontology file (mandatory)
    -i (--input) 'path_to_input_alignment'      Specifies the input alignment file (mandatory in repair mode; used as reference in match mode)
    -o (--output) 'path_to_ouput_alignment'     Specifies the output alignment file (AML will not save the result without this)
    -a (--auto)                                 Sets AML in automatic match mode (one of the three modes must be specified)
    -m (--manual)                               Sets AML in manual match mode (you can configure it in the store/config.ini file)      
    -r (--repair)                               Sets AML in repair alignment mode (requires input alignment to repair)



## AML Copyright 2013-2019 LASIGE

AML includes software developed at LASIGE (https://www.lasige.di.fc.ul.pt/) in collaboration with the ADVIS Lab (http://www.cs.uic.edu/Advis) and the Bioinformatics Unit of the IGC (http://bioinformatics.igc.gulbenkian.pt/ubi/).

If you use AML, please cite the following publication:

    D. Faria, C. Pesquita, E. Santos, M. Palmonari, I. Cruz, and F. Couto, The AgreementMakerLight ontology matching system, ODBASE 2013.
